# operators
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator 
from airflow.operators.postgres_operator import PostgresOperator

# sensors
from airflow.contrib.sensors import sftp_sensor
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.sensors.base_sensor_operator import BaseSensorOperator

# hooks
from airflow.contrib.hooks.ssh_hook import SSHHook
from airflow.hooks.postgres_hook import PostgresHook
from airflow.contrib.hooks.sftp_hook import SFTPHook
from airflow.utils.dates import days_ago
from airflow import DAG

# plugins
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults

# triggers =
from airflow.utils.trigger_rule import TriggerRule
from datetime import timedelta,datetime

# utils
import re,os,sys,logging
from paramiko import SFTP_NO_SUCH_FILE

class SFTPSensor(BaseSensorOperator):
    @apply_defaults
    def __init__(self, filepath,filepattern, sftp_conn_id='sftp_default', *args, **kwargs):
        super(SFTPSensor, self).__init__(*args, **kwargs)
        self.filepath = filepath
        self.filepattern = filepattern
        self.hook = SFTPHook(sftp_conn_id)

    def poke(self, context):
        full_path = self.filepath
        file_pattern = re.compile(self.filepattern)

        try:
            directory = self.hook.list_directory(self.hook.full_path) # watch for the files in remote location cluster 
            for files in directory:
                if not re.match(file_pattern, files):
                    self.log.info(files)
                    self.log.info(file_pattern)
                else:
                    context["task_instance"].xcom_push("file_name", files)
                    return True
            return False
        except IOError as e:
            if e.errno != SFTP_NO_SUCH_FILE:
                raise e
            return False


default_args = {
    'owner': 'synarcs',
    'email_on_failure': True,
    'retries': 3,
    'retry_delay': timedelta(minutes=2),
    'start_date': days_ago(1)
}

dag = DAG("psqlHooks",default_args=default_args,
    description="buld_pushing",
    schedule_interval=None
)

def connection():
    try:
        pg_hook = PostgresHook(postgre_conn_id="postgres_default",schema='airflow')
        connection = pg_hook.get_conn()
        cursor = connection.cursor()
        print('done connecting')
    except ConnectionError as crs:
        print('error connecting to server')

SSH_SCRIPT = '''    
            echo "poking files in an interval"
            echo "trying to connect to the server'
            if [ $? -eq "0"]
                echo "spawning the root directory
            else
                echo "Quitting the spawning process"
        ''' 

def ssh_connection():
    try:
        ssh_hook = SSHHook(ssh_conn_id='ssh_default')
        conn = ssh_hook.get_conn()
    except ConnectionError as crs:
        print('connection error !! ')

        
t00 = SSHOperator(
    task_id="execute_ssh_command",
    ssh_conn_id="ssh_default",
    command="echo hello connection success",
    dag=dag,
)

t0 = PythonOperator(task_id="ssh_hook",
            dag=dag,python_callable=ssh_connection
            ,retries=5,
            retry_delay=timedelta(seconds=2),
    )


t1 = PythonOperator(task_id="psql",trigger_rule=TriggerRule.ALL_SUCCESS,python_callable=connection,dag=dag)
t2 = PostgresOperator(
        task_id='test_db',
        postres_conn_id='postgres_default',
        trigger_rule=TriggerRule.ALL_SUCCESS,
        sql='''
            CREATE TABLE IF NOT EXISTS user (
                table_date DATE NOT NULL,
                table_value TEXT NOT NULL,
                PRIMARY KEY (table_date)
            );
        ''',
        dag=dag
)



t0 >> t00 >> t1 >> t2