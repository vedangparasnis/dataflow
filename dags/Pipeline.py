from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule
from airflow.contrib.sensors.file_sensor import FileSensor
from airflow.contrib.sensors.ftp_sensor import FTPSensor
from airflow.contrib.sensors import sftp_sensor
from airflow import DAG

import json,os
from datetime import datetime
from datetime import timedelta
import csv,sys,logging
logger = logging.getLogger("airflow.logger")

filepath = '/usr/local/airflow/dags/names.txt'

def readRules():
    if not os.path.exists('rules.csv'):
        raise AttributeError("required file not found") 
    with open('rules.csv','rt') as rules:
        data = csv.reader(rules)
        for i in range(len(data)):
            print(i)
    
default_args = {
    'owner': 'synarcs',
    'email_on_failure': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=2),
}

dag = DAG("nifiHost",default_args=default_args,
    schedule_interval="@hourly",
    description="nifiProcessDag",
    start_date=datetime.now()
)

names = []
with open(filepath,'r') as files:
    names.append(files.readlines())

def run1(**kwargs):
    this_instance = kwargs['ti']
    data = names[0]
    this_instance.xcom_push(key="current_file",data=names)

def run2(**kwargs):
    this_instance = kwargs['ti']
    data = this_instance.xcom_pull(key='current_file',task_id='select_a_file')
    import requests as rq 
    # rq.post('',{"data":data})
    this_instance.xcom_push()

t01 = DummyOperator(task_id="nothing",dag=dag)

t1 = PythonOperator(
    task_id='get_current_log_files',
    python_callable=run1,
    dag=dag,
    retries=2,
    provide_context=True,
    retry_delay=timedelta(seconds=2)
)

def log1():print('task1 just did not ran')
def log2():print('task2 just did not ran')

# for logg purpose 
t2 = PythonOperator( task_id='task_1_logger',python_callable=log1,trigger_rule=TriggerRule.ONE_FAILED,dag=dag,retries=2,
    retry_delay=timedelta(seconds=2)
)

t4 = PythonOperator( task_id='task_2_logger',python_callable=log2,trigger_rule=TriggerRule.ONE_FAILED,dag=dag,retries=2,
    retry_delay=timedelta(seconds=2)
)

# second operator
t3 = PythonOperator(
    task_id='select_a_file',
    trigger_rule=TriggerRule.ALL_SUCCESS,
    python_callable=run2,
    retries=1,
    retry_delay=timedelta(seconds=2),
    dag=dag,
)

t5 = DummyOperator(task_id="past_2_pass",trigger_rule=TriggerRule.ALL_SUCCESS,dag=dag)

filesensor = FileSensor(
    task_id="check_past_1_2",
    depends_on_past=True,
    retries_delay=timedelta(seconds=2),
    trigger_rule=TriggerRule.ALL_SUCCESS,
    filepath=filepath,
    poke_interval=3,
    dag=dag,
)

t01 >> t1
t1 >> [t2,t3]
t3 >> [t4,t5]
t5 >> filesensor
